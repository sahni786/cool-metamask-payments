
jQuery(document).ready(function ($) {
    $('#cmp-metamask-wrap .cmp-popup-paybtn').on("click", function () {
        $('.cmp-wallet-user-msg').html("");
        $('.cmp-pop-user-msg').html("");
        var min = Number(extradata.min_max.min);
        var max = Number(extradata.min_max.max);
        var price = $('#cmp-metamask-wrap input.cmp-popup-value').val();
        var custom_price = String(price );
        var user_val = String($('#cmp-metamask-wrap input.cmp-user-wallet-address').val());        
        if (typeof window.ethereum === 'undefined' || typeof web3 === 'undefined') {
            let a = document.createElement('a');
            a.target = '_blank';
            a.href = 'https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en';

            //then use this code for alert
            if (window.confirm('You need to install MetaMask extention to use this feature')) {
                a.click();
            };
            //alert('You need to install MetaMask extention to use this feature<a href="https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en">link</a>')
        }
        else {
            var meta_acoount = String(window.ethereum.selectedAddress);
            if (window.ethereum.selectedAddress == undefined) {
                alert('Please login your metamask account')
                cmp_connect();
            }
            else {
                if (extradata.restrict_payment == "yes") {
                    var check_strict = extradata.address_list;
                    var addres_list = [];
                    check_strict.forEach(cmp_custom_wallet);
                    function cmp_custom_wallet(item, index) {
                        addres_list.push(item.wallet_address);
                    }                    
                    var filarr = [];
                     $.each(extradata.cmp_custom_file, function (index, val) {
                       filarr[index] = val[0].toUpperCase();                       
                    });                    
                    if (($.inArray(user_val, addres_list) !== -1 || $.inArray(user_val.toUpperCase(), filarr) !== -1) && user_val.toUpperCase() === meta_acoount.toUpperCase()) {
                        if (price == 0) {
                            $('.cmp-pop-user-msg').html("Please Enter amount")
                        }
                        else if (min <= price && price <= max) {
                            $('.cmp-pop-user-msg').html("")
                            cmp_metamask(custom_price, extradata.receiver);
                        }
                        else {

                            $('.cmp-pop-user-msg').html("Please Enter price between " + min + '/' + max)

                        }

                    }
                    else {
                        $('.cmp-wallet-user-msg').html("Your wallet address is not whitelisted")

                    }

                }
                else {
                    if(user_val.toUpperCase() !== meta_acoount.toUpperCase()) {

                        $('.cmp-wallet-user-msg').html("Please enter valid wallet address")
                    }
                    else if (price == 0) {
                        $('.cmp-pop-user-msg').html("Please Enter amount")
                    }
                    else if (min <= price && price <= max) {
                        $('.cmp-pop-user-msg').html("")
                        $('.cmp-wallet-user-msg').html("")
                        cmp_metamask(custom_price, extradata.receiver);
                    }
                    else {
                        $('.cmp-pop-user-msg').html("Please Enter price between " + min + '/' + max)
                    }
                }
            }
        }
    })
})

function cmp_metamask(price, user_val) {
    let web3 = new Web3(Web3.givenProvider || "ws://localhost:8545");
    var user_account = user_val;
    // Let's imagine you want to receive an ether tip
    const yourAddress = user_account;
    const value = '0x' + parseInt(Web3.utils.toWei(price, 'ether')).toString(16); // an ether has 18 decimals, here in hex.
    const desiredNetwork = '56' // '1' is the Ethereum main network ID.
    // Detect whether the current browser is ethereum-compatible,
    // and handle the case where it isn't:
    if (typeof window.ethereum === 'undefined' || typeof web3 === 'undefined') {
        alert('You need to install MetaMask extention to use this feature<a href="https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en">link</a>')
    }
    else if (ethereum.networkVersion !== desiredNetwork) {
        cmp_chnage_network();
    }
    else {
        // ethereum.on('chainChanged', (_chainId) => window.location.reload());
        // In the case the user has MetaMask installed, you can easily
        // ask them to sign in and reveal their accounts:
        //   ethereum.autoRefreshOnNetworkChange = true;
        ethereum.request({ method: 'eth_requestAccounts' })
            // Remember to handle the case they reject the request:
            .catch(function (reason) {
                if (reason === 'User rejected provider access') {
                    // The user didn't want to sign in!
                } else {
                    // This shouldn't happen, so you might want to log this...
                    alert('There was an issue signing you in.')
                }
            })

            // In the case they approve the log-in request, you'll receive their accounts:
            .then(function (accounts) {
                // You also should verify the user is on the correct network:
                /*    if (ethereum.networkVersion !== desiredNetwork) {
                      alert('This application requires the main network, please switch it in your MetaMask UI.')

                      // We plan to provide an API to make this request in the near future.
                      // https://github.com/MetaMask/metamask-extension/issues/3663
                  } */

                // Once you have a reference to user accounts,
                // you can suggest transactions and signatures:

                const account = accounts[0]
                sendEtherFrom(account, function (err, transaction) {
                    if (err) {
                        return alert(`Sorry you weren't able to contribute!`)
                    }

                    alert('Thanks for your successful contribution!')
                })

            })
    }

    function sendEtherFrom(account, callback) {

        // We're going to use the lowest-level API here, with simpler example links below
        const method = 'eth_sendTransaction'
        const parameters = [{
            from: account,
            to: yourAddress,
            value: value,
            gas: '0xa028',
        }]
        const from = account

        // Now putting it all together into an RPC request:
        const payload = {
            method: method,
            params: parameters,
            from: from,
        }

        // Methods that require user authorization like this one will prompt a user interaction.
        // Other methods (like reading from the blockchain) may not.
        ethereum.sendAsync(payload, function (err, response) {
            const rejected = 'User denied transaction signature.'
            if (response.error && response.error.message.includes(rejected)) {
                return alert(`We can't take your money without your permission.`)
            }

            if (err) {
                return alert('There was an issue, please check your  meta address.')
            }

            if (response.result) {
                // If there is a response.result, the call was successful.
                // In the case of this method, it is a transaction hash.
                const txHash = response.result
                alert('Thank you for your generosity!')

                // You can poll the blockchain to see when this transaction has been mined:
                pollForCompletion(txHash, callback)
            }
        })
    }

    function pollForCompletion(txHash, callback) {
        let calledBack = false

        // Normal ethereum blocks are approximately every 15 seconds.
        // Here we'll poll every 2 seconds.
        const checkInterval = setInterval(function () {

            const notYet = 'response has no error or result'
            ethereum.sendAsync({
                method: 'eth_getTransactionByHash',
                params: [txHash],
            }, function (err, response) {
                if (calledBack) return
                if (err || response.error) {
                    if (err.message.includes(notYet)) {
                        return 'transaction is not yet mined'
                    }

                    callback(err || response.error)
                }

                // We have successfully verified the mined transaction.
                // Mind you, we should do this server side, with our own blockchain connection.
                // Client side we are trusting the user's connection to the blockchain.
                const transaction = response.result
                clearInterval(checkInterval)
                calledBack = true
                callback(null, transaction)
            })
        }, 2000)
    }
  
}




function cmp_chnage_network() {
    let ethereum = window.ethereum;
    const data = [{
        chainId: '0x38',
        chainName: 'Binance Smart Chain',
        nativeCurrency:
        {
            name: 'BNB',
            symbol: 'BNB',
            decimals: 18
        },
        rpcUrls: ['https://bsc-dataseed.binance.org/'],
        blockExplorerUrls: ['https://bscscan.com/'],
    }]
    try {
        ethereum.request({
            method: 'wallet_switchEthereumChain',
            params: [{ chainId: '0x38' }],
        });
    } catch (switchError) {
        // This error code indicates that the chain has not been added to MetaMask.
        if (switchError.code === 4902) {
            try {
                ethereum.request({
                    method: 'wallet_addEthereumChain',
                    params: data,
                });
            } catch (addError) {
                // handle "add" error
            }
        }
        // handle other "switch" errors
    }

}









function cmp_connect() {
    ethereum
        .request({ method: 'eth_requestAccounts' })        
        .catch((error) => {
            if (error.code === 4001) {
                // EIP-1193 userRejectedRequest error
                console.log('Please connect to MetaMask.');
            } else {
                console.error(error);
            }
        });
}