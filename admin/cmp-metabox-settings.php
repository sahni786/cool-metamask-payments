<?php
/**
 * 
 * This file is responsible for creating all admin settings in Timeline Builder (post)
 */
if( !defined("ABSPATH") ){
  exit('Can not load script outside of WordPress Enviornment!');
}

if (!class_exists('CMP_metabox_settings')) {
  class CMP_metabox_settings {


/**
     * The unique instance of the plugin.
     *
     */
    private static $instance;

    /**
     * Gets an instance of our plugin.
     *
     */
    public static function get_instance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }


      /**
       * The Constructor
       */
      public function __construct() {
          // register actions
          $this->CMP_metabox_settings();
          $this->tets();
        //  add_action('admin_print_styles', array($this, 'ect_custom_shortcode_style'));

      }

        public function ect_custom_shortcode_style()
        {
            echo '<style>span.dashicon.dashicons.dashicons-ect-custom-icon:before {
        content:"";
        background: url(' . CMP_PLUGIN_URL . 'assets/images/ect-icon.png);
        background-size: contain;
        background-repeat: no-repeat;
        height: 20px;
        display: block;

        }</style>';
        }


public function CMP_metabox_settings(){

    // Control core classes for avoid errors
if (class_exists('CSF')) {

    //
    // Set a unique slug-like ID
    $prefix = 'cmp_coolmetamask';

    //
    // Create a metabox
   
    $account_transient = get_transient("cmp-acount-balance");
    $blnc=!empty($account_transient)?number_format(($account_transient->result/1000000000000000000),4):"";
    CSF::createMetabox($prefix, array(
        'title' => 'Coolmetamask Payments',
        'post_type' => 'cmp',        
        'nav'=>'inline',
        
    ));

        CSF::createSection($prefix, array(              
            'title'  => __('Account Settings'),
            'icon'   => 'fas fa-rocket',        
            'fields' => array(
                
                array(
                    'id' => 'receiver',
                    'type' => 'text',
                    'title' => 'Receiver Address',
                    'placeholder' => 'Enter Receiver address',                    
                ),              
                array(
                    'id'     => 'min_max',
                    'type'   => 'fieldset',
                    'title'  => 'Min/max range',
                    'fields' => array(
                        array(
                        'id'    => 'min',
                        'type'  => 'number',
                        'title' => 'Min',
                        'default'=>$blnc,
                        ),
                        array(
                        'id'    => 'max',
                        'type'  => 'number',
                        'title' => 'Max',
                        ),                       
                    ),
                    ),
                 array(
                    'id' => 'raised',
                    'type' => 'text',
                    'title' => 'Add amount to raised',      
                    'placeholder' => 'Enter amount to raise',                                   
                    'attributes'=>array(
                        'style'       => 'width: 60%;',
                    ),
                ),      
                 
                 array(
                    'id' => 'input_custom_msg',
                    'type' => 'text',
                    'title' => 'Input field message',
                    'placeholder' => 'Enter wallet address',                     
                    'attributes'=>array(
                        'style'       => 'width: 60%;',
                    ),
            
                ),
                  array(
                    'id' => 'popup_custom_msg',
                    'type' => 'text',
                    'title' => 'Popup message',
                    'placeholder' => 'Enter amount',                     
                    'attributes'=>array(
                        'style'       => 'width: 60%;',
                    ),
            
                ),
                 array(
                    'id' => 'paynow_btn_txt',
                    'type' => 'text',
                    'title' => 'Button text',
                    'default'=>'Pay Now',
                    'attributes'=>array(
                        'style'       => 'width: 60%;',
                    ),
            
                ),
                array(
                    'id' => 'restrict_payment',
                    'type' => 'radio',
                    'title' => 'Restrict Payments',
                    'options'    => array(
                        'yes' => 'Yes',
                        'no' => 'No',
                        
                    ),
                    'default'=>'no',
                   
            
                ),
                 array(
                    'id'    => 'cmp_updated_wallets',
                    'type'  => 'upload',
                    'title' => 'Upload CSV file',
                    'desc'=>'Upload your csv file to restric users',
                    'dependency' => array( 'restrict_payment', '==', 'yes' ),
                    ),
                 array(
                    'id'     => 'address_list',
                    'type'   => 'repeater',
                    'title'  => 'Add Wallets',      
                    'dependency' => array( 'restrict_payment', '==', 'yes' ),              
                    'fields' => array(
                        array(
                        'id'    => 'wallet_address',
                        'type'  => 'text',
                        'title' => 'Address'
                        ),                      
                    ),
                     'default'   => array(
                        array(
                        'wallet_address' => 'Enter wallet address',
                        
                        ),
                        
                    )
                    ),  
                
                
                
            ),
        ));

       




       

}}

function tets(){
    if (class_exists('CSF')) {

    //
    // Set a unique slug-like ID
    $prefix = 'cmp_registration';

    /**
     *
     * @menu_parent argument examples.
     *
     * For Dashboard: 'index.php'
     * For Posts: 'edit.php'
     * For Media: 'upload.php'
     * For Pages: 'edit.php?post_type=page'
     * For Comments: 'edit-comments.php'
     * For Custom Post Types: 'edit.php?post_type=your_post_type'
     * For Appearance: 'themes.php'
     * For Plugins: 'plugins.php'
     * For Users: 'users.php'
     * For Tools: 'tools.php'
     * For Settings: 'options-general.php'
     *
     */
    CSF::createOptions($prefix, array(
        'framework_title'         => 'Cool Metamask Account Registration',
        'menu_title' => 'Registration',
        'menu_slug' => 'Registration',
        'menu_type' => 'submenu',
        'menu_parent' => 'edit.php?post_type=cmp',
        'show_search'             => false,
    'show_reset_all'          => false,
    'show_reset_section'      => false,
    'theme'                   => 'light',
    ));

 CSF::createSection($prefix, array(
    'title' => __('API Registrstion', 'cptbx'),
    'icon' => 'fas fa-pencil-ruler',
    'fields' => array(
        // Layouts Options

        array(
            'id' => 'api_key',
            'type' => 'text',
            'title' => 'API Key',
            'placeholder' => 'Enter Api key',
            'desc'=>'<a href="https://docs.bscscan.com/getting-started/viewing-api-usage-statistics" target="_blank">Get API KEY</a>',
        ),      

    ),
));


}

}




    
}}

new CMP_metabox_settings();
