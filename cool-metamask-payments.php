<?php
/*
Plugin Name:Cool metamask payment
Plugin URI:https://coolplugins.net/
Description:meta mask payments.
Version:1.0
Requires at least: 4.5
Tested up to:5.8
Requires PHP:5.6
Stable tag:trunk
Author:Cool Plugins
Author URI:https://coolplugins.net/
License URI:https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: /languages
Text Domain:cmp
 */

if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}
if (!defined('CMP_VERSION')) {
    define('CMP_VERSION', '1.0');
}
/*** Defined constent for later use */
define('CMP_PLUGIN_URL', plugin_dir_url(__FILE__));
define('CMP_PLUGIN_DIR', plugin_dir_path(__FILE__));
/*** Coolmetamask_payments main class by CoolPlugins.net */
if (!class_exists('Coolmetamask_payments')) {
    final class Coolmetamask_payments
    {

        /**
         * The unique instance of the plugin.
         *
         */
        private static $instance;

        /**
         * Gets an instance of our plugin.
         *
         */
        public static function get_instance()
        {
            if (null === self::$instance) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        /**
         * Constructor.
         */
        private function __construct()
        {

        }

        // register all hooks
        public function registers()
        {

            /*** Installation and uninstallation hooks */
            register_activation_hook(__FILE__, array('Coolmetamask_payments', 'activate'));
            register_deactivation_hook(__FILE__, array('Coolmetamask_payments', 'deactivate'));

            /*** Load required files */
            add_action('plugins_loaded', array(self::$instance, 'cmp_load_files'));
            add_action('init', array(self::$instance, 'cmp_post_type'));

            //  add_shortcode('coolmetamask',array(self::$instance,'cmp_shortcode'));
            add_filter('manage_cmp_posts_columns', array(self::$instance, 'cmp_custom_columns'));
            // this fills in the columns that were created with each individual post's value
            add_action('manage_cmp_posts_custom_column', array(self::$instance, 'cmp_custom_columns_data'), 10, 2);
            add_action('add_meta_boxes', array(self::$instance, 'register_cmp_meta_box'));

        }

/*
|--------------------------------------------------------------------------
| Post meta for CMC shortcodes
|--------------------------------------------------------------------------
 */
        public function register_cmp_meta_box()
        {
            add_meta_box('cmp-shortcode', 'Coolmetamask shortcode', array(self::$instance, 'cmp_shortcode_meta'), 'cmp', 'side', 'high');
        }

        public function cmp_shortcode_meta()
        {
            $id = get_the_ID();
            $dynamic_attr = '';
            echo '<p>Paste this shortcode in anywhere (page/post)</p>';
            $dynamic_attr .= "[coolmetamask id=\"{$id}\"";
            $dynamic_attr .= ']';
            ?>
            <input type="text" class="cmc-regular-small" style="width:100%;text-align:center;" onClick="this.select();" name="cmc_meta_box_text" id="cmc_meta_box_text" value="<?php echo htmlentities($dynamic_attr); ?>" readonly/>
            <?php
}

        public function cmp_custom_columns_data($column, $id)
        {
            switch ($column) {
                case 'shortcode':
                    echo '<input type="text" style="width:100%;text-align:center;" value="[coolmetamask id=' . $id . ']" onClick="this.select();" readonly>';
                    break;

            }
        }

        public function cmp_custom_columns($columns)
        {

            $date = $columns['date'];
            unset($columns['date']);
            $columns['shortcode'] = __('Shortcode', 'cptbx');
            $columns['date'] = $date;
            return $columns;
        }

        /*** Load required files */
        public function cmp_load_files()
        {

            require_once CMP_PLUGIN_DIR . 'includes/cmp-functions.php';
            if (is_admin()) {
                /*** Plugin review notice file */
                require_once CMP_PLUGIN_DIR . 'admin/codestar/codestar-framework.php';
                require_once CMP_PLUGIN_DIR . 'admin/cmp-metabox-settings.php';
            }
            require_once CMP_PLUGIN_DIR . 'includes/cmp-shortcode.php';

          


        }

        /*
        |--------------------------------------------------------------------------
        | Register Custom Post Type of Crypto Widget
        |--------------------------------------------------------------------------
         */
        public function cmp_post_type()
        {

// Set UI labels for Custom Post Type
            $labels = array(
                'name' => _x('Coolmetamask Payments', 'Post Type General Name', 'twentytwenty'),
                'singular_name' => _x('Coolmetamask Payments', 'Post Type Singular Name', 'twentytwenty'),
                'menu_name' => __('Coolmetamask', 'twentytwenty'),
                'parent_item_colon' => __('Parent Item:', 'ccpwx'),
                'all_items' => __('All Shortcodes', 'ccpwx'),
                'add_new_item' => __('Add New Shortcode', 'ccpwx'),
                'add_new' => __('Add New', 'ccpwx'),
                'new_item' => __('New Item', 'ccpwx'),
                'edit_item' => __('Edit Item', 'ccpwx'),
                'update_item' => __('Update Item', 'ccpwx'),
                'view_item' => __('View Item', 'ccpwx'),
                'view_items' => __('View Items', 'ccpwx'),
                'search_items' => __('Search Item', 'ccpwx'),
                'not_found' => __('Not found', 'ccpwx'),
                'not_found_in_trash' => __('Not found in Trash', 'ccpwx'),
                'featured_image' => __('Featured Image', 'ccpwx'),
                'set_featured_image' => __('Set featured image', 'ccpwx'),
                'remove_featured_image' => __('Remove featured image', 'ccpwx'),
                'use_featured_image' => __('Use as featured image', 'ccpwx'),
                'insert_into_item' => __('Insert into item', 'ccpwx'),
                'uploaded_to_this_item' => __('Uploaded to this item', 'ccpwx'),
                'items_list' => __('Items list', 'ccpwx'),
                'items_list_navigation' => __('Items list navigation', 'ccpwx'),
                'filter_items_list' => __('Filter items list', 'ccpwx'),
            );

// Set other options for Custom Post Type

            $args = array(
                'label' => __('Coolmetamask Payments', 'twentytwenty'),
                'description' => __('Post Type Description', 'twentytwenty'),
                'labels' => $labels,
                // Features this CPT supports in Post Editor
                'supports' => array('title'),
                'taxonomies' => array(''),
                /* A hierarchical CPT is like Pages and can have
                 * Parent and child items. A non-hierarchical CPT
                 * is like Posts.
                 */
                'hierarchical' => true,
                'public' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'show_in_nav_menus' => true,
                'show_in_admin_bar' => true,
                'menu_position' => 5,
                'can_export' => true,
                'has_archive' => true,
                'exclude_from_search' => false,
                'publicly_queryable' => true,
                'capability_type' => 'post',
                'show_in_rest' => true,

            );

// Registering your Custom Post Type
            register_post_type('cmp', $args);

        }

    }

}
/*** Coolmetamask_payments main class - END */

/*** THANKS - CoolPlugins.net ) */
$ect = Coolmetamask_payments::get_instance();
$ect->registers();
