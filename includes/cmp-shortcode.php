<?php

function cmp_shortcode($atts)
{
    $attr = shortcode_atts(array(
        'id' => '',
        'input_custom_msg' => 'something else',
        'min' => '0.5',
        'max' => '5',
        'type' => '',
    ), $atts);
    $post_id = $attr['id'];   
    wp_enqueue_script('cmp-cussdton-js', CMP_PLUGIN_URL . 'assets/js/web3.min.js', array('jquery'), CMP_VERSION, true);
    wp_enqueue_script('cmp-custon-js', CMP_PLUGIN_URL . 'assets/js/cmp-custom.js', array('jquery','cmp-cussdton-js'), CMP_VERSION, true);   
    wp_enqueue_style('cmp-custom-css', CMP_PLUGIN_URL . '/assets/css/cmp-custom.css', null, CMP_VERSION);  
    $meta = get_post_meta($post_id, 'cmp_coolmetamask', true);  
    $file_nm=!empty($meta['cmp_updated_wallets']) ? cmp_read_csv($meta['cmp_updated_wallets']) : "";  
    $meta['cmp_custom_file']=$file_nm;      
    wp_localize_script('cmp-custon-js', 'extradata', $meta);
     $options = get_option('cmp_registration');            
    $api_key = !empty($options['api_key']) ? $options['api_key'] : "";
    $api_wallet = !empty($meta['receiver']) ? $meta['receiver'] : "";            
    $account_transient = cmp_get_account_balance($api_key,$api_wallet);    
    $blnc=!empty($account_transient)?number_format($account_transient,4):"0";
    $input_msg = !empty($meta['input_custom_msg']) ? $meta['input_custom_msg'] : "";
    $popup_msg = !empty($meta['popup_custom_msg']) ? $meta['popup_custom_msg'] : "";
    $min = !empty($meta['min_max']['min']) ? $meta['min_max']['min'] : $blnc;
    $raised = !empty($meta['raised']) ? $meta['raised'] : "";
    $btn_txt = !empty($meta['paynow_btn_txt']) ? $meta['paynow_btn_txt'] : "";     
    $html = "";

    $html .= '<div class="cmp-metamask-wrapper" id="cmp-metamask-wrap">
                        <span class="cmp-minmax">' . $blnc . '/' . $raised . ' BNB Raised</span>
                            <span class="cmp-input-msg">' . $input_msg . '</span>
                            <input type="text" class="cmp-user-wallet-address" ><span class="cmp-wallet-user-msg"></span>
                            <span class="cmp-input-msg">' . $popup_msg . '</span>
                            <input type="text" class="cmp-popup-value" id="cmp-popup-value" ><span class="cmp-pop-user-msg"></span>
                         <button class="cmp-popup-paybtn">' . $btn_txt . '</button> ';

    $html .= '</div>';
    return $html;

}
function cmp_read_csv($file_nm){
        if (($open = fopen($file_nm, "r")) !== false) {

    while (($data = fgetcsv($open, 1000, ",")) !== false) {
        $array[] = $data;
    }

    fclose($open);
}

return $array;

}

add_shortcode('coolmetamask',  'cmp_shortcode');

