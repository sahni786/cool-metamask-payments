<?php
if (!defined("ABSPATH")) {
    exit('Can not load script outside of WordPress Enviornment!');
}


 function cmp_get_account_balance($api,$wallet)
{
    if (empty($api)|| empty($wallet)){
        return;
    }
    $expiry_time = 10 * MINUTE_IN_SECONDS ;
    $account_transient = get_transient("cmp-acount-balance");    
    if (empty($account_transient) || $account_transient === "") {
        $request = wp_remote_get('https://api.bscscan.com/api?module=account&action=balance&address='.$wallet.'&apikey='.$api.'', array('timeout' => 120, 'sslverify' => false));
        if (is_wp_error($request)) {
            return false; // Bail early
        }
        $body = wp_remote_retrieve_body($request);
        $balance = json_decode($body);

        if (!empty($balance->error)) {
            return;
        }
        if (!empty($balance )) {
            set_transient("cmp-acount-balance", $balance , $expiry_time);
            return ($balance->result/1000000000000000000);

        }
    }
    else{
        return ($account_transient->result/1000000000000000000);
    }

}